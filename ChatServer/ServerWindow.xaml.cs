﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Threading;
using System.Windows;
using System.Windows.Input;

namespace ChatServer {
    /// <summary>
    /// Interaction logic for ServerWindow.xaml
    /// </summary>
    public partial class ServerWindow : Window {
        private static readonly string ServerName = Environment.MachineName;
        private readonly ConcurrentQueue<string> messageQueue = new ConcurrentQueue<string>();
        private readonly ConcurrentQueue<string> stateChangeQueue = new ConcurrentQueue<string>();
        private readonly BackgroundWorker worker = new BackgroundWorker();
        private bool serverRunning;
        private Server server;

        private readonly List<IPAddress> addresses = Dns.GetHostEntry(ServerName).AddressList
            .Where(address => address.AddressFamily == AddressFamily.InterNetwork).ToList();

        public ServerWindow() {
            InitializeComponent();

            // Initialize Background Worker
            worker.WorkerReportsProgress = true;
            worker.ProgressChanged += Worker_ProgressChanged;
            worker.DoWork += Worker_DoWork;

            worker.RunWorkerAsync();

            foreach (IPAddress ipAddress in addresses)
                AddressCmbx.Items.Add(ipAddress);
        }

        private void Server_ReceivedMessage(string nick, string message) {
            if (string.IsNullOrWhiteSpace(message) == false)
                messageQueue.Enqueue($"{nick}: {message.Trim()}");
        }

        private void Server_ClientStateChanged(string nick, ClientState state, string oldNick = "") {
            switch (state) {
                case ClientState.Connected:
                    stateChangeQueue.Enqueue($"[ADD] {nick}");
                    break;
                case ClientState.Disconnected:
                    stateChangeQueue.Enqueue($"[REM] {nick}");
                    break;
                case ClientState.NameChange:
                    stateChangeQueue.Enqueue($"[ADD] {nick}");
                    stateChangeQueue.Enqueue($"[REM] {oldNick}");
                    break;
                default:
                    throw new ArgumentOutOfRangeException(nameof(state), state, null);
            }
        }

        private void Worker_DoWork(object sender, DoWorkEventArgs e) {
            while (worker.CancellationPending == false) {
                worker.ReportProgress(0);
                Thread.Sleep(100);
            }
        }

        private void Worker_ProgressChanged(object sender, ProgressChangedEventArgs e) {
            // Dump out if server's not running, since we don't have anything to update then.
            if (server == null) {
                return;
            }
            
            // Update the UI
            UptimeLbl.Content = $"Uptime: {Server.ConvertTimeSpanToString(DateTime.Now - server.ServerStart)}";

            while (messageQueue.IsEmpty == false) {
                if (messageQueue.TryDequeue(out string message))
                    MessagesLsbx.Items.Add(message);
            }

            while (stateChangeQueue.IsEmpty == false) {
                if (!stateChangeQueue.TryDequeue(out string stateChange))
                    continue;

                string[] temp = stateChange.Split(' ');
                string change = temp.First();
                string value = temp.Last();

                switch (change) {
                    case "[ADD]":
                        if (value == Server.ServerNick) {
                            // Server just started
                            ClientsLsbx.Items.Clear();

                            break;
                        }

                        ClientsLsbx.Items.Add(value);
                        break;
                    case "[REM]":
                        if (value == Server.ServerNick) {
                            // Server just stopped
                            ClientsLsbx.Items.Clear();

                            break;
                        }

                        ClientsLsbx.Items.Remove(value);
                        break;
                }
            }
        }

        private void StarServerBtn_OnClick(object sender, RoutedEventArgs e) {
            if (serverRunning) {
                StarServerBtn.Content = "Start Server";
                UptimeLbl.Visibility = Visibility.Hidden;
                serverRunning = false;
                server.Stop();
                return;
            }

            if (int.TryParse(PortTxt.Text, out int port) == false) {
                MessagesLsbx.Items.Add("Please enter a valid port!");
                return;
            }

            if (!(AddressCmbx.SelectedValue is IPAddress address))
                return;

            TcpListener listener = new TcpListener(address, port);
            listener.Start();

            server = new Server(listener);
            server.ReceivedMessage += Server_ReceivedMessage;
            server.ClientStateChanged += Server_ClientStateChanged;

            server.Start();
            serverRunning = true;

            MessagesLsbx.Items.Clear();
            StarServerBtn.Content = "Stop Server";
            UptimeLbl.Visibility = Visibility.Visible;

            MessagesLsbx.Items.Add($"Server listening on {address}:{port}");
        }

        private void PortTxt_OnPreviewTextInput(object sender, TextCompositionEventArgs e) {
            if (int.TryParse(e.Text, out int _) == false)
                e.Handled = true;
        }

        private void PortTxt_OnPasting(object sender, DataObjectPastingEventArgs e) {
            if (e.DataObject.GetDataPresent(typeof(string))) {
                string text = e.DataObject.GetData(typeof(string)) as string;
                if (int.TryParse(text, out int _) == false)
                    e.CancelCommand();
            } else {
                e.CancelCommand();
            }
        }

        private void SendBtn_OnClick(object sender, RoutedEventArgs e) {
            if (serverRunning == false) {
                MessagesLsbx.Items.Add("Server isn't running!");
                return;
            }

            try {
                string input = MessageTxt.Text.Trim();
                server.SendMessageFromServer(input);
            } catch (NullReferenceException) {
                MessagesLsbx.Items.Add("Server isn't running!");
            } finally {
                MessageTxt.Clear();
            }
        }
    }
}
