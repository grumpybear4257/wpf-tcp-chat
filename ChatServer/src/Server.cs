﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.IO;
using System.Linq;
using System.Net.Sockets;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Formatters.Binary;
using System.Threading.Tasks;

namespace ChatServer {
    public enum ClientState {
        Connected,
        Disconnected,
        NameChange
    }

    public class Server {
        public delegate void ReceivedMessageHandler(string nick, string message);
        public delegate void ClientStateChange(string nick, ClientState state, string oldNick = "");
        public event ReceivedMessageHandler ReceivedMessage;
        public event ClientStateChange ClientStateChanged;

        public DateTime ServerStart { get; private set; }
        public const string ServerNick = "SERVER";

        private readonly Dictionary<TcpClient, string> clientNicknames = new Dictionary<TcpClient, string>();
        private readonly BackgroundWorker worker = new BackgroundWorker();
        private readonly IFormatter formatter = new BinaryFormatter();
        private readonly TcpListener listener;

        // Format is item1 = command name, item2 = command description, item3 = command usage
        private static readonly List<Tuple<string, string, string>> Commands = new List<Tuple<string, string, string>>();

        static Server() {
            // Initialize commands
            Commands.Add(new Tuple<string, string, string>("Help", "View a list of all commands (You are here!)", "/help"));
            Commands.Add(new Tuple<string, string, string>("Nick", "Change your nickname", "/nick {nickname}"));
            Commands.Add(new Tuple<string, string, string>("List", "View a list of all currently online clients", "/list"));
            Commands.Add(new Tuple<string, string, string>("Uptime", "Shows the amount of time the server has been running", "/uptime"));
            Commands.Add(new Tuple<string, string, string>("Message", "Send a message to only the specified user", "/msg {receiver nickname} {message}"));
        }

        public Server(TcpListener listener) {
            this.listener = listener;

            worker.WorkerSupportsCancellation = true;
            worker.DoWork += Worker_DoWork;
        }

        public static string ConvertTimeSpanToString(TimeSpan timeSpan) {
            string days, hours;
            if (timeSpan.Days > 1)
                days = $"{timeSpan.Days} days";
            else if (timeSpan.Days == 0)
                days = string.Empty;
            else
                days = $"{timeSpan.Days} day";

            if (timeSpan.Hours > 1)
                hours = $"{timeSpan.Hours} hours";
            else if (timeSpan.Days == 0 && timeSpan.Hours == 0)
                hours = string.Empty;
            else
                hours = $"{timeSpan.Hours} hour";

            string minutes = timeSpan.Minutes > 1 ? $"{timeSpan.Minutes} minutes" : $"{timeSpan.Minutes} minute";
            string seconds = timeSpan.Seconds > 1 ? $"{timeSpan.Seconds} seconds" : $"{timeSpan.Seconds} second";

            if (string.IsNullOrEmpty(days) && string.IsNullOrEmpty(hours) == false)
                return $"{hours}, {minutes}, {seconds}";

            if (string.IsNullOrEmpty(hours))
                return $"{minutes}, {seconds}";

            return $"{days}, {hours}, {minutes}, {seconds}";
        }

        public void Start() {
            worker.RunWorkerAsync();
            ClientStateChanged?.Invoke(ServerNick, ClientState.Connected);

            ServerStart = DateTime.Now;
        }

        public void Stop() {
            HandleNewMessage(ServerNick, "Stopping server!");
            ClientStateChanged?.Invoke(ServerNick, ClientState.Disconnected);

            foreach (KeyValuePair<TcpClient, string> pair in clientNicknames)
                pair.Key.Client.Close();

            worker.CancelAsync();
            listener.Stop();

            ServerStart = DateTime.MinValue;
        }

        public void SendMessageFromServer(string message) {
            // Process server command (currently only kick)
            if (message.StartsWith("/")) {
                string[] rawCommand = message.Remove(0, 1).Trim().Split(' ');
                string command = rawCommand[0].ToLower();
                string[] parameters = rawCommand.Skip(1).ToArray();

                switch (command) {
                    case "kick":
                        // Make sure they entered someone to kick
                        if (parameters.Length == 0) {
                            ReceivedMessage?.Invoke("Syntax Error", "Please enter a nickname to kick! (usage: '/kick {nickname}')");
                            break;
                        }

                        // Go through all the nicks they entered (eg '/kick Nathan Nigel' will kick both Nathan and Nigel) and validate them
                        foreach (string nick in parameters) {
                            if (clientNicknames.ContainsValue(nick)) {
                                TcpClient clientToKick = clientNicknames.First(client => client.Value == nick).Key;
                                BinaryWriter writer = new BinaryWriter(clientToKick.GetStream());
                                SendMessageToClient(writer, ServerNick, "You have been kicked from the server!");

                                clientToKick.Client.Close();
                                // writers.Remove(writer);

                                HandleNewMessage(ServerNick, $"{nick} was kicked from the server!");
                            } else {
                                // Inform invoker (server) that the nick they entered wasn't found
                                ReceivedMessage?.Invoke("Invalid parameter", $"Unable to find client with nickname {nick}!");
                            }
                        }

                        break;
                }

                return;
            }

            HandleNewMessage(ServerNick, message);
        }

        private async void Worker_DoWork(object sender, DoWorkEventArgs e) {
            // Start accepting connections
            while (worker.CancellationPending == false) {
                try {
                    TcpClient connection = listener.AcceptTcpClient();

                    Task<string> getNickTask = Task.Run(() => GetNickName(connection));
                    string nick = await getNickTask;

                    if (string.IsNullOrWhiteSpace(nick))
                        continue;

                    clientNicknames.Add(connection, nick);
                    HandleNewMessage(ServerNick, $"{nick} has joined the chat.");
                    ClientStateChanged?.Invoke(nick, ClientState.Connected);

                    #pragma warning disable 4014 // we don't want to wait for this method to finish, the whole point of it is to NOT finish until the client disconnects
                    Task.Run(() => HandleClient(connection));
                    #pragma warning restore 4014
                } catch (Exception ex) {
                    if (ex.Message.Contains("WSACancelBlockingCall"))
                        return;

                    ReceivedMessage?.Invoke("MAIN::ERROR", ex.Message);
                }
            }
        }

        private string GetNickName(TcpClient client) {
            NetworkStream stream = client.GetStream();
            BinaryWriter writer = new BinaryWriter(stream);
            BinaryReader reader = new BinaryReader(stream);

            string nick = string.Empty;
            while (string.IsNullOrWhiteSpace(nick)) {
                try {
                    formatter.Serialize(writer.BaseStream, "Please enter a nickname");
                    nick = formatter.Deserialize(new BinaryReader(stream).BaseStream).ToString().Trim();

                    if (clientNicknames.ContainsValue(nick)) {
                        formatter.Serialize(writer.BaseStream, "That nickname is already taken!");
                        nick = string.Empty;
                        continue;
                    }

                    if (nick.Trim().Contains(' ')) {
                        formatter.Serialize(writer.BaseStream, "Nicknames can only be one word!");
                        nick = string.Empty;
                        continue;
                    }

                    if (string.IsNullOrWhiteSpace(nick))
                        continue;

                    formatter.Serialize(writer.BaseStream, $"Your nickname will be: '{nick}'. Is that OK? (y/n)");

                    string confirm = formatter.Deserialize(reader.BaseStream).ToString().Trim();
                    if (confirm != "y" && confirm != "yes")
                        nick = string.Empty;
                } catch (Exception) {
                    // Do nothing
                    return string.Empty;
                }
            }

            formatter.Serialize(writer.BaseStream, $"Welcome to the server, {nick}! Do /help for a list of commands!");

            return nick;
        }

        private void HandleClient(TcpClient client) {
            NetworkStream socketStream = client.GetStream();
            BinaryWriter writer = new BinaryWriter(socketStream);
            BinaryReader reader = new BinaryReader(socketStream);

            while (worker.CancellationPending == false && client.Connected) {
                string nick = "Something went wrong...";
                try {
                    nick = clientNicknames[client];
                    object data = formatter.Deserialize(reader.BaseStream);

                    // Could also check for other types, like a file
                    if (!(data is string message))
                        continue;

                    // Handle commands 
                    if (message.StartsWith("/")) {
                        string[] rawCommand = message.Remove(0, 1).Trim().Split(' ');
                        string command = rawCommand[0].ToLower();
                        string[] parameters = rawCommand.Skip(1).ToArray();

                        switch (command) {
                            case "nick":
                                if (parameters.Length == 0) {
                                    formatter.Serialize(writer.BaseStream,
                                        "Please enter a nickname! (usage: '/nick {nickname}')");

                                    continue;
                                }

                                if (parameters.Length > 1) {
                                    formatter.Serialize(writer.BaseStream, "Nicknames can only be one word!");
                                    continue;
                                }

                                ChangeNick(parameters[0].Trim(), writer, client);

                                continue;
                            case "help":
                                formatter.Serialize(writer.BaseStream, "The commands available on this server are:");
                                foreach ((string name, string description, string usage) in Commands)
                                    formatter.Serialize(writer.BaseStream,
                                        $"{name}: {description}\nUsage: {usage}");

                                continue;
                            case "list":
                                formatter.Serialize(writer.BaseStream, "The following clients are currently online:");
                                foreach (KeyValuePair<TcpClient, string> clientNick in clientNicknames)
                                    formatter.Serialize(writer.BaseStream, clientNick.Value);

                                continue;
                            case "uptime":
                                formatter.Serialize(writer.BaseStream, $"The current uptime is: {ConvertTimeSpanToString(DateTime.Now - ServerStart)}");
                                continue;
                            case "whisper":
                            case "w":
                            case "dm":
                            case "msg":
                                string receiver = parameters[0];
                                string whisper = string.Join(" ", parameters.Skip(1));

                                // ReSharper disable once SwitchStatementMissingSomeCases
                                switch (parameters.Length) {
                                    case 0:
                                        // Check to see if they ran the command with no params, and tell them they need to specify a user
                                        formatter.Serialize(writer.BaseStream, "Please enter a user to send the message to.");
                                        continue;
                                    case 1:
                                        // Check to see if they only specified a user, and tell them they need to send a message
                                        formatter.Serialize(writer.BaseStream, $"Please enter a message to send to {receiver}.");
                                        continue;
                                }

                                // Check to see if they're sending a message to themselves.
                                if (receiver == nick) {
                                    formatter.Serialize(writer.BaseStream, "Unfortunately you can't message yourself.");
                                    continue;
                                }

                                HandlePrivateMessage(nick, receiver, whisper);

                                continue;
                            default:
                                formatter.Serialize(writer.BaseStream, $"Unknown command '/{command}'");

                                continue;
                        }
                    }

                    HandleNewMessage(nick, message.Trim());
                } catch (Exception) {
                    client.Close();
                    client.Dispose();
                    clientNicknames.Remove(client);
                    HandleNewMessage(ServerNick, $"{nick} has disconnected");
                    ClientStateChanged(nick, ClientState.Disconnected);
                }
            }
        }

        private void ChangeNick(string newNick, BinaryWriter writer, TcpClient client) {
            string oldNick = clientNicknames[client];

            // Check to make sure the nick isn't taken
            if (clientNicknames.ContainsValue(newNick)) {
                formatter.Serialize(writer.BaseStream, "That nickname is already taken!");
                return;
            }

            if (string.Equals(newNick, ServerNick, StringComparison.CurrentCultureIgnoreCase) ||
                string.Equals(newNick, "admin", StringComparison.CurrentCultureIgnoreCase)) {
                formatter.Serialize(writer.BaseStream, "Nice try ;)");
                return;
            }

            clientNicknames[client] = newNick;
            formatter.Serialize(writer.BaseStream, $"Nickname successfully changed to: {newNick}");

            HandleNewMessage("SERVER", $"{oldNick} is now known as {newNick}");
            ClientStateChanged?.Invoke(newNick, ClientState.NameChange, oldNick);
        }

        private void SendMessageToClient(BinaryWriter writer, string senderNick, string message) {
            formatter.Serialize(writer.BaseStream, $"{senderNick}： {message}");
        }

        private void HandleNewMessage(string nick, string message) {
            if (string.IsNullOrWhiteSpace(message))
                return;

            try {
                foreach (KeyValuePair<TcpClient, string> pair in clientNicknames) {
                    BinaryWriter writer = new BinaryWriter(pair.Key.GetStream());
                    SendMessageToClient(writer, nick, message);
                }
            } catch (Exception ex) {
                ReceivedMessage?.Invoke("MSG::ERROR", ex.Message);
            }

            ReceivedMessage?.Invoke(nick, message);
        }

        private void HandlePrivateMessage(string senderNick, string receiverNick, string message) {
            // Get the TCPClient of the sender, it shouldn't be null, unless something else is very wrong.
            TcpClient sender = clientNicknames.First(snd => snd.Value == senderNick).Key;
            BinaryWriter senderWriter = new BinaryWriter(sender.GetStream());

            // Check to see if the target user is online.
            if (clientNicknames.ContainsValue(receiverNick) == false) {
                // Send a message to the sender saying the person they're trying to message isn't connected.
                SendMessageToClient(senderWriter, ServerNick, $"Couldn't find '{receiverNick}'! Make sure you spelled the username right, otherwise they probably aren't connected right now.");

                return;
            }

            // Get the tcp connection of the target
            TcpClient receiver = clientNicknames.First(rec => rec.Value == receiverNick).Key;
            BinaryWriter receiverWriter = new BinaryWriter(receiver.GetStream());

            // Send the message to the recipient
            SendMessageToClient(receiverWriter, $"{senderNick} -> ME", message);
            // Send the message back to the sender so they can see that it sent
            SendMessageToClient(senderWriter, $"ME -> {receiverNick}", message);
            // Log the message in the console 
            ReceivedMessage?.Invoke($"{senderNick} -> {receiverNick}", message);
        }
    }
}
