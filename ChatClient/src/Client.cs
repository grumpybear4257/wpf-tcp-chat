﻿using System;
using System.IO;
using System.Net;
using System.Net.Sockets;
using System.ComponentModel;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Formatters.Binary;

namespace ChatClient {
    public class Client {
        public delegate void ReceivedMessageHandler(string message);
        public event ReceivedMessageHandler ReceivedMessage;

        private readonly BackgroundWorker worker = new BackgroundWorker();
        private IFormatter formatter = new BinaryFormatter();
        private readonly IPAddress ipAddress;
        private readonly int port;

        private TcpClient client;
        private NetworkStream stream;
        private BinaryReader reader;
        private BinaryWriter writer;

        public Client(IPAddress ipAddress, int port) {
            worker.WorkerSupportsCancellation = true;
            worker.WorkerReportsProgress = true;
            worker.DoWork += Worker_DoWork;
            worker.ProgressChanged += Worker_ProgressChanged;

            worker.RunWorkerAsync();

            this.ipAddress = ipAddress;
            this.port = port;
        }

        public void SendMessage(string message) {
            try {
                formatter.Serialize(writer.BaseStream, message);
            } catch (Exception ex) {
                Console.WriteLine(ex);
                throw;
            }
        }

        public void Disconnect() {
            worker.CancelAsync();
            client.Close();

            formatter = null;
        }

        private void Worker_DoWork(object sender, DoWorkEventArgs e) {
            try {
                client = new TcpClient();
                client.Connect(ipAddress, port);

                stream = client.GetStream();
                reader = new BinaryReader(stream);
                writer = new BinaryWriter(stream);

                worker.ReportProgress(0, $"CONNECTED TO - {ipAddress}:{port}");
            } catch (Exception) {
                worker.ReportProgress(-1);
                return;
            }

            try {
                while (worker.CancellationPending == false) {
                    object data = formatter.Deserialize(stream);

                    if (data is string message)
                        worker.ReportProgress(0, message);
                }

                client.Close();
            } catch (Exception ex) {
                Console.WriteLine(ex);
                worker.ReportProgress(99);
            } finally {
                writer.Close();
                reader.Close();
                stream.Close();
                client.Close();
                worker.CancelAsync();
            }
        }

        private void Worker_ProgressChanged(object sender, ProgressChangedEventArgs e) {
            if (e.ProgressPercentage == 99)
                ReceivedMessage?.Invoke("Disconnected");
            else if (e.ProgressPercentage == -1) 
                ReceivedMessage?.Invoke("Unable to connect to server!");
            else
                ReceivedMessage?.Invoke(e.UserState as string);
        }
    }
}
