﻿using System;
using System.ComponentModel;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Windows;
using System.Windows.Input;
using Notifications.Wpf;

namespace ChatClient {
    /// <summary>
    /// Interaction logic for ClientWindow.xaml
    /// </summary>
    public partial class ClientWindow : Window {
        private Client client;
        private bool connected;
        private string nick = "__NOTSET__";
        private string lastSentMessage = string.Empty;
        private readonly IPAddress myIp = Dns.GetHostEntry(Dns.GetHostName()).AddressList
            .First(ip => ip.AddressFamily == AddressFamily.InterNetwork);
        private readonly NotificationManager nm = new NotificationManager();

        public ClientWindow() {
            InitializeComponent();

            // Set defaults for the server ip and port fields (default to localhost)
            ServerTxt.Text = myIp.ToString();
            PortTxt.Text = 5000.ToString();
        }

        private void ConnectBtn_OnClick(object sender, RoutedEventArgs e) {
            if (connected) {
                // Disconnect from the server
                ConnectBtn.Content = "Connect";

                client.Disconnect();
                connected = false;
                client = null;
            } else {
                // Connect to the sever
                MessagesLsbx.Items.Clear();
                if (IPAddress.TryParse(ServerTxt.Text, out IPAddress ip)) {
                    if (ip.AddressFamily == AddressFamily.InterNetwork) {
                        if (int.TryParse(PortTxt.Text, out int port)) {
                            client = new Client(ip, port);
                            client.ReceivedMessage += Client_ReceivedMessage;
                            connected = true;

                            ConnectBtn.Content = "Disconnect";

                            return;
                        }

                        MessagesLsbx.Items.Add("Please enter a valid port!");
                        return;
                    }
                }

                MessagesLsbx.Items.Add("Please enter a valid IPv4 address!");
            }
        }

        private void Client_ReceivedMessage(string message) {
            if (message == "Unable to connect to server!" || message == "Disconnected") {
                ConnectBtn.Content = "Connect";
                connected = false;
                client = null;

                MessagesLsbx.Items.Add(message);
                return;
            }

            string formattedMessage = $"{DateTime.Now:h:mm:ss tt} - {message}";
            MessagesLsbx.Items.Add(formattedMessage);
            MessagesLsbx.ScrollIntoView(formattedMessage);

            // This is a pretty bad way to do it, but it keeps the server compatible with other clients (in theory), however, my client will super break if the message format isn't the same. 
            // Check if the message is telling us our nickname, and set the nick to that. (HOLY CRAP THIS IS SUCH A BAD WAY TO IMPLEMENT IT LMAO)
            if (message.Contains("Your nickname will be: '"))
                nick = message.Substring(message.IndexOf('\'') + 1, message.LastIndexOf('\'') - message.IndexOf('\'') - 1);

            // Holy crap more bad plz look away dont judge me
            if (message.Contains("：") == false)
                return;

            string msgNick = message.Substring(0, message.IndexOf('：'));
            if (msgNick == nick || nick == "__NOTSET__")
                return;

            nm.Show(new NotificationContent {
                Title = "New Message",
                Message = message,
                Type = NotificationType.Information
            });
        }

        private void PortTxt_OnPreviewTextInput(object sender, TextCompositionEventArgs e) {
            if (int.TryParse(e.Text, out int _) == false)
                e.Handled = true;
        }

        private void PortTxt_OnPasting(object sender, DataObjectPastingEventArgs e) {
            if (e.DataObject.GetDataPresent(typeof(string))) {
                string text = e.DataObject.GetData(typeof(string)) as string;
                if (int.TryParse(text, out int _) == false)
                    e.CancelCommand();
            } else {
                e.CancelCommand();
            }
        }

        private void SendBtn_Click(object sender, RoutedEventArgs e) {
            try {
                string input = MessageTxt.Text.Trim();
                client.SendMessage(input);

                lastSentMessage = input;
            } catch (NullReferenceException) {
                MessagesLsbx.Items.Add("You aren't connected to a server!");
            } finally {
                MessageTxt.Clear();
            }
        }

        private void MessageTxt_OnKeyUp(object sender, KeyEventArgs e) {
            switch (e.Key) {
                // Check to see if they hit the up arrow key, or the page up key, if they did, put the last message they sent in the textbox
                case Key.Up:
                case Key.PageUp:
                    // If we wanted to go even further, we could have a list of all messages sent, and cycle through that,
                    // but I don't really see a point in going that far.
                    MessageTxt.Text = lastSentMessage;
                    MessageTxt.SelectionStart = MessageTxt.Text.Length;
                    break;
                case Key.Down:
                case Key.PageDown:
                    // Set the textbox to empty 
                    MessageTxt.Text = string.Empty;
                    break;
            }
        }

        private void ClientWindow_OnClosing(object sender, CancelEventArgs e) {
            client?.Disconnect();
            Application.Current.Shutdown();
        }

        private void ClearBtn_OnClick(object sender, RoutedEventArgs e) {
            MessagesLsbx.Items.Clear();
        }
    }
}